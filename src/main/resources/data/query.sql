-- number of films by year
SELECT EXTRACT(YEAR FROM f.release_date) AS year, COUNT(f.id) AS number_of_films
FROM film f
GROUP BY year
ORDER BY year;

-- number of films by director
SELECT CONCAT(d.first_name, ' ', d.last_name) AS director, COUNT(f.id) AS number_of_films
FROM director d
         JOIN film f on d.id = f.director_id
GROUP BY director
ORDER BY number_of_films DESC;

-- directors without film
SELECT CONCAT(d.first_name, ' ', d.last_name) AS director, d.birth_date
FROM director d
         LEFT JOIN film f on d.id = f.director_id
WHERE f.id IS NULL;

-- 5 oldest director with their films
SELECT CONCAT(d.first_name, ' ', d.last_name) AS director,
       d.birth_date,
       f.name                                 AS film,
       f.release_date,
       g.genre_name                           AS genre
FROM director d
         JOIN film f on d.id = f.director_id
         JOIN genre g on g.id = f.genre_id
ORDER BY d.birth_date
    LIMIT 5;

-- films starting from 2010, but not drama
SELECT f.name AS film, g.genre_name AS genre, EXTRACT(YEAR FROM f.release_date) AS year
FROM film f
    JOIN genre g on g.id = f.genre_id
WHERE f.release_date >= '2010-01-01'
    EXCEPT
SELECT f.name AS film, g.genre_name AS genre, EXTRACT(YEAR FROM f.release_date) AS year
FROM film f
    JOIN genre g on g.id = f.genre_id
WHERE g.genre_name = 'drama';