CREATE DATABASE postgres;

DROP VIEW IF EXISTS film_info;
DROP TABLE IF EXISTS film;
DROP TABLE IF EXISTS director;
DROP TABLE IF EXISTS genre;

CREATE TABLE director
(
    id         BIGSERIAL PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL,
    birth_date DATE         NOT NULL
);

CREATE TABLE genre
(
    id         BIGSERIAL PRIMARY KEY,
    genre_name VARCHAR(35)
);

CREATE TABLE film
(
    id           BIGSERIAL PRIMARY KEY,
    director_id  INTEGER REFERENCES director (id) NOT NULL,
    name         VARCHAR(255),
    release_date DATE,
    genre_id     INTEGER REFERENCES genre (id)    NOT NULL
);

-- Indexes:
CREATE INDEX film_idx ON film (name);
CREATE INDEX director_last_name_idx ON director (last_name);

-- View

CREATE VIEW film_info AS
SELECT f.name AS film, g.genre_name AS genre, CONCAT(d.first_name, ' ', d.last_name) AS director
FROM film f
         JOIN director d on d.id = f.director_id
         JOIN genre g on g.id = f.genre_id
ORDER BY name;