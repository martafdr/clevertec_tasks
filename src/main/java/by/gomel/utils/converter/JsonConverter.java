package by.gomel.utils.converter;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.logging.Logger;

public class JsonConverter {

    private static final Logger logger = Logger.getLogger(JsonConverter.class.getName());

    public String convert(Object objectToConvert) {
        Optional<Object> optional = Optional.ofNullable(objectToConvert);
        return optional.isPresent() ? String.format("{ \n%s}", convertObjectToJson(objectToConvert)) : "{ \n}";
    }

    private String convertObjectToJson(Object objectToConvert) {
        Field[] fields = objectToConvert.getClass().getDeclaredFields();
        int length = fields.length;
        StringBuilder builder = new StringBuilder();

        for (Field field : fields) {
            field.setAccessible(true);
            builder.append(convertFieldToJson(field, objectToConvert));
            //add ',' and go to next line if it isn't the last field of converted object
            if (field != fields[length - 1]) {
                builder.append(",\n");
            } else builder.append('\n');
        }
        return builder.toString();
    }

    private String convertFieldToJson(Field field, Object objectToConvert) {
        String convertedField = "";
        try {
            String fieldName = field.getName();
            String fieldValue = field.get(objectToConvert).toString();
            if (field.getType().isPrimitive())
                convertedField = String.format("  \"%s\": %s", fieldName, fieldValue);
            else if (field.getType() == String.class || field.getType() == Enum.class)
                convertedField = String.format("  \"%s\": \"%s\"", fieldName, fieldValue);
        } catch (IllegalAccessException e) {
            logger.info("An illegal reflective access operation has occurred");
        }
        return convertedField;
    }

    public String getSchema(Object object) {
        Optional<Object> optional = Optional.ofNullable(object);
        if(!optional.isPresent())
            return "{ \n}";

        Field[] fields = object.getClass().getDeclaredFields();
        StringBuilder sbFields = new StringBuilder();
        int fieldsLength = fields.length;

        for (Field field : fields) {
            field.setAccessible(true);
            sbFields.append(convertFieldForSchema(field));
            //add ',' and go to next line if it isn't the last field of converted object
            if (field != fields[fieldsLength - 1])
                sbFields.append(",\n");
        }
        return String.format("{ \n \"title\": \"%s\",\n \"type\": \"object\",\n \"properties\": {\n%s\n }\n}",
                object.getClass().getSimpleName(), sbFields);
    }

    private String convertFieldForSchema(Field field) {
        String typeData = field.getType().getSimpleName().toLowerCase();
        if (typeData.equals("int")) {
            typeData = "integer";
        }
        if (typeData.equals("byte") || typeData.equals("short") || typeData.equals("long")
                || typeData.equals("double") || typeData.equals("float")) {
            typeData = "number";
        }
        return String.format("  \"%s\": {\n    \"type\": \"%s\"\n  }", field.getName(), typeData);
    }
}