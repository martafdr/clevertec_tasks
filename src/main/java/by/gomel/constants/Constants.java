package by.gomel.constants;

public class Constants {
    public final static String FILE_WITH_PROPERTIES = "src/main/resources/application.yml";
    public final static String FILE_NOT_FOUND = "File not found!";
    public final static String CONNECT_ERROR = "Can not connect to the SQL server";

    public final static String CACHE_TYPE = "cacheType";
    public final static String CACHE_CAPACITY = "cacheCapacity";

    public final static String DB_DRIVER = "dbDriver";
    public final static String DB_URL = "dbUrl";
    public final static String DB_USER = "dbUser";
    public final static String DB_PASSWORD = "dbPassword";

    public final static String DELIMITER = ";";

    public final static String MESSAGE_EX_IN_SAVE = "Inappropriate data in save method";
    public final static String MESSAGE_EX_IN_GET = "Inappropriate data in get method";
    public final static String MESSAGE_EX_IN_UPDATE = "Inappropriate data in update method";
}
