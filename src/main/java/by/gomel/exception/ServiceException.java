package by.gomel.exception;

/**
 * ServiceException
 */
public class ServiceException extends RuntimeException {

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.
     */
    public ServiceException() {
    }

    /**
     * Constructs a new runtime exception with the specified detail message.
     */
    public ServiceException(String message) {
        super(message);
    }
}
