package by.gomel.exception;

/**
 * NoSuchDatabaseElementException
 * necessary for situation, then element not found in Database
 */
public class NoSuchDatabaseElementException extends RuntimeException {

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.
     */
    public NoSuchDatabaseElementException() {
    }

    /**
     * Constructs a new runtime exception with the specified detail message.
     */
    public NoSuchDatabaseElementException(String message) {
        super(message);
    }
}
