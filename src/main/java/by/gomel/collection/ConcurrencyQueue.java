package by.gomel.collection;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class ConcurrencyQueue<E> implements Collection<E> {

    private static final int DEFAULT_INITIAL_CAPACITY = 11;

    /**
     * The maximum size of array to allocate.
     */
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    /**
     * ConcurrencyQueue is a collection designed for holding elements prior
     * to processing.
     */
    Object[] queue; // non-private to simplify nested class access

    /**
     * The number of elements in the queue.
     */
    private int size;

    /**
     * Creates a ConcurrencyQueue with the default initial capacity (11)
     */
    public ConcurrencyQueue() {
        this(DEFAULT_INITIAL_CAPACITY);
    }

    /**
     * Creates a ConcurrencyQueue with the specified initial capacity.
     *
     * @throws IllegalArgumentException if initialCapacity is less than 1
     */
    public ConcurrencyQueue(int initialCapacity) {
        if (initialCapacity < 1)
            throw new IllegalArgumentException();
        this.queue = new Object[initialCapacity];
    }

    /**
     * Returns the number of elements in this queue.
     */
    public int size() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return size;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Lock for operations, with which can operate different threats (all public operations)
     */
    private final ReentrantLock lock = new ReentrantLock();

    /**
     * Returns {@code true} if this queue contains no elements.
     */
    public boolean isEmpty() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return size == 0;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Returns {@code true} if this queue contains the specified element.
     */
    public boolean contains(Object o) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return indexOf(o) >= 0;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Increases the capacity by 50%.
     */
    private void grow() {
        int oldCapacity = queue.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        // capacity can not be more than max array size
        if (newCapacity > MAX_ARRAY_SIZE) {
            newCapacity = MAX_ARRAY_SIZE;
        }
        queue = Arrays.copyOf(queue, newCapacity);
    }

    /**
     * Inserts the specified element into this queue.
     *
     * @return {@code true} (as specified by {@link Collection#add})
     * @throws ClassCastException   if the class of the specified element
     *                              prevents it from being added to this queue
     * @throws NullPointerException if the specified element is null
     */
    public boolean add(E e) {
        return offer(e);
    }

    /**
     * Inserts the specified element into this queue.
     *
     * @return {@code true} (as specified by {@link Queue#offer})
     * @throws ClassCastException   if the class of the specified element
     *                              prevents it from being added to this queue
     * @throws NullPointerException if the specified element is null
     */
    public boolean offer(E e) {
        if (e == null)
            throw new NullPointerException();
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            int i = size;
            if (i >= queue.length)
                grow();
            Object[] es = queue;
            siftUpComparable(i, e, queue);
            size = i + 1;
        } finally {
            lock.unlock();
        }
        return true;
    }

    /**
     * Inserts item x at position k.
     */
    private static <T> void siftUpComparable(int k, T x, Object[] es) {
        Comparable<? super T> key = (Comparable<? super T>) x;
        while (k > 0) {
            int parent = (k - 1) >>> 1;
            Object e = es[parent];
            if (key.compareTo((T) e) >= 0)
                break;
            es[k] = e;
            k = parent;
        }
        es[k] = key;
    }

    /**
     * Removes and returns the first element from this queue.
     *
     * @throws NoSuchElementException if this queue is empty
     */
    public E remove() {
        E result = poll();
        if (!Optional.ofNullable(result).isPresent())
            throw new NoSuchElementException();
        return result;
    }

    /**
     * Removes and returns the first element from this queue.
     * or returns {@code null} if this queue is empty.
     */
    public E poll() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            final Object[] es = queue;
            final E result = (E) (es[0]);
            if (result != null) {
                final int n = --size;
                final E x = (E) es[n];
                es[n] = null;
                if (n > 0) {
                    siftDownComparable(0, x, es, n);
                }
            }
            return result;
        } finally {
            lock.unlock();
        }
    }

    private static <T> void siftDownComparable(int k, T x, Object[] es, int n) {
        // assert n > 0;
        Comparable<? super T> key = (Comparable<? super T>) x;
        int half = n >>> 1;
        while (k < half) {
            int child = (k << 1) + 1;
            Object c = es[child];
            int right = child + 1;
            if (right < n &&
                    ((Comparable<? super T>) c).compareTo((T) es[right]) > 0)
                c = es[child = right];
            if (key.compareTo((T) c) <= 0)
                break;
            es[k] = c;
            k = child;
        }
        es[k] = key;
    }

    public boolean remove(Object o) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            int i = indexOf(o);
            if (i == -1)
                return false;
            else {
                removeAt(i);
                return true;
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Identity-based version for use in Itr.remove.
     *
     * @param o element to be removed from this queue, if present
     */
    private void removeEq(Object o) {
        final Object[] es = queue;
        for (int i = 0; i < size; i++) {
            if (o.equals(es[i])){
                removeAt(i);
                break;
            }
        }
    }

    /**
     * Removes the ith element from queue.
     */
    private E removeAt(int i) {
        // assert i >= 0 && i < size;
        final Object[] es = queue;
        int s = --size;
        if (s == i)
            es[i] = null;
        else {
            E moved = (E) es[s];
            es[s] = null;
            siftDownComparable(i, moved, queue, s);
            if (es[i] == moved) {
                siftUpComparable(i, moved, queue);
                if (es[i] != moved)
                    return moved;
            }
        }
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return c.stream().allMatch(this::contains);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            boolean modified = false;
            for (E e : c)
                if (add(e))
                    modified = true;
            return modified;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Objects.requireNonNull(c);
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            boolean modified = false;
            Iterator<?> it = iterator();
            while (it.hasNext()) {
                if (c.contains(it.next())) {
                    it.remove();
                    modified = true;
                }
            }
            return modified;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Objects.requireNonNull(c);
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            boolean modified = false;
            Iterator<E> it = iterator();
            while (it.hasNext()) {
                if (!c.contains(it.next())) {
                    it.remove();
                    modified = true;
                }
            }
            return modified;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Retrieves, but does not remove, the head of this queue.  This method
     * differs from {@link #peek peek} only in that it throws an exception
     * if this queue is empty.
     *
     * @return the head of this queue
     * @throws NoSuchElementException if this queue is empty
     */
    public E element() {
        if (!Optional.ofNullable(peek()).isPresent()) {
            throw new NoSuchElementException();
        }
        return peek();
    }

    /**
     * Retrieves, but does not remove, the head of this queue,
     * or returns {@code null} if this queue is empty.
     */
    public E peek() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return (E) queue[0];
        } finally {
            lock.unlock();
        }
    }

    /**
     * Find index of the specified object
     * or returns -1 if this queue don't have this object
     */
    private int indexOf(Object o) {
        if(!Optional.ofNullable(o).isPresent())
            return -1;
        final Object[] es = queue;
        return IntStream.range(0, es.length).filter(i -> o.equals(es[i])).findFirst().orElse(-1);
    }

    /**
     * Removes all of the elements from this queue.
     */
    public void clear() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            final Object[] es = queue;
            for (int i = 0, n = size; i < n; i++)
                es[i] = null;
            size = 0;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Returns an iterator over the elements in this queue. The iterator
     * does not return the elements in any particular order.
     *
     * @return an iterator over the elements in this queue
     */
    @Override
    public Iterator<E> iterator() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return new ConcurrencyQueue.Itr();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Returns an array containing all of the elements in this queue.
     */
    @Override
    public Object[] toArray() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return Arrays.copyOf(queue, size);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Returns an array containing all of the elements in this queue; the
     * runtime type of the returned array is that of the specified array.
     *
     * @throws ArrayStoreException  if the runtime type of the specified array
     *                              is not a supertype of the runtime type of every element in
     *                              this queue
     * @throws NullPointerException if the specified array is null
     */
    @Override
    public <T> T[] toArray(T[] a) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            final int size = this.size;
            if (a.length < size)
                return (T[]) Arrays.copyOf(queue, size, a.getClass());
            System.arraycopy(queue, 0, a, 0, size);
            if (a.length > size)
                a[size] = null;
            return a;
        } finally {
            lock.unlock();
        }
    }

    private final class Itr implements Iterator<E> {
        /**
         * Index (into queue array) of element to be returned by
         * subsequent call to next.
         */
        private int cursor;

        /**
         * Index of element returned by most recent call to next,
         * unless that element came from the forgetMeNot list.
         * Set to -1 if element is deleted by a call to remove.
         */
        private int lastRet = -1;

        /**
         * A queue of elements that were moved from the unvisited portion of
         * the heap into the visited portion as a result of "unlucky" element
         * removals during the iteration.
         */
        private ArrayDeque<E> forgetMeNot;

        /**
         * Element returned by the most recent call to next iff that
         * element was drawn from the forgetMeNot list.
         */
        private E lastRetElt;

        Itr() {
        }

        public boolean hasNext() {
            return cursor < size ||
                    (forgetMeNot != null && !forgetMeNot.isEmpty());
        }

        public E next() {
            if (cursor < size)
                return (E) queue[lastRet = cursor++];
            if (forgetMeNot != null) {
                lastRet = -1;
                lastRetElt = forgetMeNot.poll();
                if (lastRetElt != null)
                    return lastRetElt;
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            if (lastRet != -1) {
                E moved = ConcurrencyQueue.this.removeAt(lastRet);
                lastRet = -1;
                if (moved == null)
                    cursor--;
                else {
                    if (forgetMeNot == null)
                        forgetMeNot = new ArrayDeque<>();
                    forgetMeNot.add(moved);
                }
            } else if (lastRetElt != null) {
                ConcurrencyQueue.this.removeEq(lastRetElt);
                lastRetElt = null;
            } else {
                throw new IllegalStateException();
            }
        }
    }

    /**
     * Performs the given action for each element of queue
     * until all elements have been processed or the action throws an exception.
     * Actions are performed in the order of iteration, if that order is specified.
     *
     * @throws NullPointerException if the specified action is null
     */
    @Override
    public void forEach(Consumer<? super E> action) {
        Objects.requireNonNull(action);
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            final Object[] es = queue;
            for (int i = 0; i < size; i++)
                action.accept((E) es[i]);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Returns a string representation of this queue.
     */
    public String toString() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            Iterator<E> it = iterator();
            StringBuilder sb = new StringBuilder();
            if (!it.hasNext())
                return "[]";
            else sb.append('[').append(it.next());

            while (it.hasNext()) {
                sb.append(", ").append(it.next());
            }
            return sb.append(']').toString();
        } finally {
            lock.unlock();
        }
    }
}