package by.gomel.dbConnector;

import by.gomel.constants.Constants;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * ConnectionPool for connection to Database
 */
public class ConnectionPool {
    private final Logger log = LoggerFactory.getLogger(ConnectionPool.class);

    /**
     * driver for Database
     */
    private String driver;

    /**
     * url for connection to Database
     */
    private String url;

    /**
     * user's login for Database
     */
    private String user;

    /**
     * user's password for Database
     */
    private String password;

    /**
     * create a c3p0 pooling DataSource
     */
    private final ComboPooledDataSource dataSource = new ComboPooledDataSource();

    /**
     * instance of connection pool
     */
    private static ConnectionPool connectionPool;

    /**
     * Constructor for connection pool with max size 10
     */
    private ConnectionPool() {
        try {
            initProperties();

            dataSource.setDriverClass(driver);
            dataSource.setJdbcUrl(url);
            dataSource.setUser(user);
            dataSource.setPassword(password);

            dataSource.setMaxPoolSize(10);
        } catch (Exception e) {
            log.error(e.toString());
            throw new RuntimeException(Constants.CONNECT_ERROR);
        }
    }

    /**
     * Returns connection pool
     */
    public static ConnectionPool getInstance() {
        if (connectionPool == null) {
            connectionPool = new ConnectionPool();
        }
        return connectionPool;
    }

    /**
     * Returns Connection
     */
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    /**
     * Initialize properties (driver, url, user, password)
     */
    private void initProperties() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(Constants.FILE_WITH_PROPERTIES));
            driver = properties.getProperty(Constants.DB_DRIVER);
            url = properties.getProperty(Constants.DB_URL);
            user = properties.getProperty(Constants.DB_USER);
            password = properties.getProperty(Constants.DB_PASSWORD);
        } catch (FileNotFoundException e) {
            log.error(Constants.FILE_NOT_FOUND);
        } catch (IOException e) {
            log.error(e.toString());
        }
    }
}
