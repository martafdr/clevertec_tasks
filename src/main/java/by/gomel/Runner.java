package by.gomel;

import by.gomel.entity.Director;
import by.gomel.entity.Film;
import by.gomel.entity.Genre;
import by.gomel.repository.impl.DirectorDao;
import by.gomel.repository.impl.FilmDao;
import by.gomel.repository.impl.GenreDao;
import by.gomel.service.EntityService;
import by.gomel.service.impl.EntityServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;

public class Runner {
    public static void main(String[] args) {
        final Logger logger = LoggerFactory.getLogger(Runner.class);
        final String BEFORE_SAVING_IN_DB = "Object before saving in DB: ";
        final String AFTER_SAVING_IN_DB = "Object after saving in DB: ";
        final String AFTER_UPDATE_IN_DB = "Object after update in DB: ";

        EntityService<Director> directorService = new EntityServiceImpl<>(new DirectorDao());
        // create director with mistake first name
        Director director = new Director("Piter", "Farrelly", Date.valueOf("1956-12-17"));
        logger.info(BEFORE_SAVING_IN_DB + director);
        // save it in DB
        directorService.save(director);
        logger.info(AFTER_SAVING_IN_DB + director);
        // update
        director.setFirstName("Peter");
        directorService.update(director);
        logger.info(AFTER_UPDATE_IN_DB + directorService.get(director.getId()));

        EntityService<Genre> genreService = new EntityServiceImpl<>(new GenreDao());
        // create genre with mistake name
        Genre genre = new Genre("tragicomedi");
        logger.info(BEFORE_SAVING_IN_DB + genre);
        // save it in DB
        genreService.save(genre);
        logger.info(AFTER_SAVING_IN_DB + genre);
        // update
        genre.setName("tragicomedy");
        genreService.update(genre);
        logger.info(AFTER_UPDATE_IN_DB + genreService.get(genre.getId()));

        EntityService<Film> filmService = new EntityServiceImpl<>(new FilmDao());
        // create film with mistake name
        Film film = new Film(director, "Red Book", Date.valueOf("2018-09-11"), genre);
        logger.info(BEFORE_SAVING_IN_DB + film);
        // save it in DB
        filmService.save(film);
        logger.info(AFTER_SAVING_IN_DB + film);
        // update
        film.setName("Green Book");
        filmService.update(film);
        logger.info(AFTER_UPDATE_IN_DB + filmService.get(film.getId()));

        filmService.delete(film.getId());
    }
}