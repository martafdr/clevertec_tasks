package by.gomel.service.impl;

import by.gomel.aspectj.CacheableMode;
import by.gomel.constants.Constants;
import by.gomel.exception.ServiceException;
import by.gomel.repository.EntityDao;
import by.gomel.service.EntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * Implementation of Entity service
 * with CRUD operations and using CacheableMode annotation,
 * which reduces the number of queries to the database by using the cache
 */
public class EntityServiceImpl<T> implements EntityService<T> {
    private final Logger log = LoggerFactory.getLogger(EntityServiceImpl.class);
    private final EntityDao<T> dao;

    public EntityServiceImpl(EntityDao<T> dao) {
        this.dao = dao;
    }

    @CacheableMode
    @Override
    public long save(T item) {
        try {
            return dao.save(item);
        } catch (SQLException e) {
            log.error(e.toString());
        }
        throw new ServiceException(Constants.MESSAGE_EX_IN_SAVE);
    }

    @CacheableMode
    @Override
    public T get(long id) {
        try {
            return (T) dao.get(id);
        } catch (SQLException e) {
            log.error(e.toString());
        }
        throw new ServiceException(Constants.MESSAGE_EX_IN_GET);
    }

    @CacheableMode
    @Override
    public long update(T item) {
        try {
            return dao.update(item);
        } catch (SQLException e) {
            log.error(e.toString());
        }
        throw new ServiceException(Constants.MESSAGE_EX_IN_UPDATE);
    }

    @CacheableMode
    @Override
    public void delete(long id) {
        try {
            dao.delete(id);
        } catch (SQLException e) {
            log.error(e.toString());
        }
    }
}