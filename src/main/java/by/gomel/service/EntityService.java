package by.gomel.service;

public interface EntityService<T> {
    long save(T item);

    T get(long id);

    long update(T item);

    void delete(long id);
}
