package by.gomel.aspectj.impl;

import by.gomel.cache.AbstractCache;
import by.gomel.factory.CacheFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation Cacheable Mode annotation
 */
@Aspect
public class CacheableModeImpl {
    private final Logger logger = LoggerFactory.getLogger(CacheableModeImpl.class);

    /**
     * Create cache by using Cache Factory
     */
    private final AbstractCache<Object, Object> cache = new CacheFactory<>().getCacheFromFactory();

    /**
     * Save information in cache
     * used in methods (* save(..)) with annotation @CacheableMode
     */
    @AfterReturning(pointcut = ("@annotation(by.gomel.aspectj.CacheableMode) && execution(* save(..))"), returning = "result")
    public void save(JoinPoint joinPoint, Object result) throws Throwable {
        Object[] args = joinPoint.getArgs();

        for (Object obj : args) {
            if (obj != null) {
                cache.put(result, obj);
                logger.debug(obj + " is saved in cache");
            }
        }
    }

    /**
     * Update information in cache
     * used in methods (* update(..)) with annotation @CacheableMode
     */
    @AfterReturning(pointcut = ("@annotation(by.gomel.aspectj.CacheableMode) && execution(* update(..))"), returning = "result")
    public void update(JoinPoint joinPoint, Object result) throws Throwable {
        Object[] args = joinPoint.getArgs();

        for (Object obj : args) {
            if (obj != null) {
                cache.update(result, obj);
                logger.debug(obj + " is updated in cache");
            }
        }
    }

    /**
     * Get information from cache if it exists. If not, get it from dao
     * used in methods (* get(..)) with annotation @CacheableMode
     */
    @Around("@annotation(by.gomel.aspectj.CacheableMode) && execution(* get(..))")
    public Object getFromCacheOrDao(ProceedingJoinPoint joinPoint) throws Throwable {
        Object daoObj = null;
        Object[] args = joinPoint.getArgs();

        for (Object obj : args) {
            if (obj != null) {
                if (cache.get(obj) != null) {
                    daoObj = cache.get(obj);
                    logger.debug(daoObj + " is gotten from cache");
                } else {
                    daoObj = joinPoint.proceed();
                    logger.debug(daoObj != null ? daoObj + " is gotten from dao" : "Not found!");
                }
            }
        }
        return daoObj;
    }

    /**
     * Delete information in cache if it exists and not null.
     * used in methods (* delete(..)) with annotation @CacheableMode
     */
    @Around("@annotation(by.gomel.aspectj.CacheableMode) && execution(* delete(..))")
    public Object remove(ProceedingJoinPoint joinPoint) throws Throwable {
        Object daoObj;
        Object[] args = joinPoint.getArgs();

        for (Object obj : args) {
            if (obj != null && cache.get(obj) != null) {
                daoObj = cache.get(obj);
                cache.remove(obj);
                logger.debug(daoObj + " is removed from cache");
            }
        }
        return joinPoint.proceed();
    }
}