package by.gomel.factory;

import by.gomel.cache.AbstractCache;
import by.gomel.cache.LfuCache;
import by.gomel.cache.LruCache;
import by.gomel.constants.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Factory for creation cache
 */
public class CacheFactory<K, V> {
    private final Logger log = LoggerFactory.getLogger(CacheFactory.class);

    /**
     * type of necessary cache
     */
    private String type;

    /**
     * capacity of necessary cache
     */
    private int capacity;

    /**
     * Returns cache with type and capacity which initialized in initProperties();
     * if type or capacity is incorrect or absent, created LRU cache with capacity 10
     */
    public AbstractCache<K, V> getCacheFromFactory() {
        initProperties();
        AbstractCache<K, V> cache;
        try {
            cache = CacheKind.valueOf(type).getCache(capacity);
        } catch (IllegalArgumentException e) {
            log.error(e.toString());
            // default used LRU cache with capacity 10
            cache = new LruCache<K, V>(10);
        }
        return cache;
    }

    /**
     * Enumeration with existing types of cache and their realization of getCache(int capacity)
     */
    private enum CacheKind {
        LRU {
            <K, V> AbstractCache<K, V> getCache(int capacity) {
                return new LruCache<K, V>(capacity);
            }
        }, LFU {
            <K, V> AbstractCache<K, V> getCache(int capacity) {
                return new LfuCache<K, V>(capacity);
            }
        };

        abstract <K, V> AbstractCache<K, V> getCache(int capacity);
    }

    /**
     * Initialize properties (type and capacity)
     */
    private void initProperties() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(Constants.FILE_WITH_PROPERTIES));
            type = properties.getProperty(Constants.CACHE_TYPE);
            capacity = Integer.parseInt(properties.getProperty(Constants.CACHE_CAPACITY));
        } catch (FileNotFoundException e) {
            log.error(Constants.FILE_NOT_FOUND);
        } catch (IOException | NumberFormatException e) {
            log.error(e.toString());
        }
    }
}