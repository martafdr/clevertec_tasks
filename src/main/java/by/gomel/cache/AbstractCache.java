package by.gomel.cache;

public abstract class AbstractCache<K, V> {

    public abstract void put(K key, V value);

    public abstract V get(K key);

    public abstract void update(K key, V value);

    public abstract void remove(K key);
}