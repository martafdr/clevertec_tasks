package by.gomel.cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * LfuCache is the implementation of LFU cache. Least Frequently Used (LFU) is a caching algorithm
 * in which the least frequently used cache block is removed whenever the cache is overflowed.
 */
public class LfuCache<K, T> extends AbstractCache<K, T> {

    /**
     * Cache is a Map designed for storage data
     */
    private final Map<K, CacheEntry> cacheMap;

    /**
     * The number of elements in the cache.
     */
    private final int size;

    /**
     * Constructs with the specified initial capacity.
     *
     * @throws IllegalArgumentException if the initial capacity is negative
     */
    public LfuCache(int capacity) {
        size = capacity;
        if (capacity > 0) {
            cacheMap = new LinkedHashMap<>(capacity);
        } else {
            throw new IllegalArgumentException("capacity of cache can not be negative");
        }
    }

    /**
     *
     * Save the value in cache with the specified key. If cache is full remove one element,
     * which was the least frequently used.
     */
    @Override
    public void put(K key, T item) {
        if (isFull())
            cacheMap.remove(getLfuKey());

        CacheEntry temp = new CacheEntry();
        temp.setItem(item);
        temp.setFrequency(0);

        cacheMap.put(key, temp);
    }

    /**
     * Returns the key of element, which was the least frequently used.
     */
    private K getLfuKey() {
        K key = null;
        int minFreq = Integer.MAX_VALUE;

        for (Map.Entry<K, CacheEntry> entry : cacheMap.entrySet()) {
            if (minFreq > entry.getValue().frequency) {
                key = entry.getKey();
                minFreq = entry.getValue().frequency;
            }
        }

        return key;
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or {@code null} if cache contains no mapping for the key.
     */
    @Override
    public T get(K key) {
        if (cacheMap.containsKey(key)) {
            CacheEntry temp = cacheMap.get(key);
            temp.frequency++;
            cacheMap.put(key, temp);
            return temp.item;
        }
        return null;
    }

    /**
     * Update the value in cache to which the specified key is mapped.
     */
    @Override
    public void update(K key, T item) {
        if (cacheMap.containsKey(key)) {
            CacheEntry temp = cacheMap.get(key);
            temp.frequency++;
            temp.setItem(item);
            cacheMap.put(key, temp);
        }
    }

    /**
     * Removes the element of cache to which the specified key is mapped.
     */
    @Override
    public void remove(K key) {
        cacheMap.remove(key);
    }

    /**
     * Returns {@code true} if cache is full.
     */
    public boolean isFull() {
        return cacheMap.size() == size;
    }

    /**
     * Returns the number of elements in cache.
     */
    public int size() {
        return cacheMap.size();
    }

    /**
     * Returns {@code true} if cache contains no elements.
     */
    public boolean isEmpty() {
        return cacheMap.isEmpty();
    }

    /**
     * Returns a string representation of cache.
     */
    @Override
    public String toString() {
        return cacheMap.toString();
    }

    /**
     * Support class for cache entry, which contains the item and frequency of using this item
     */
    private class CacheEntry {
        private T item;
        private int frequency;

        public T getItem() {
            return item;
        }

        public void setItem(T item) {
            this.item = item;
        }

        public int getFrequency() {
            return frequency;
        }

        public void setFrequency(int frequency) {
            this.frequency = frequency;
        }

        @Override
        public String toString() {
            return item.toString();
        }
    }
}