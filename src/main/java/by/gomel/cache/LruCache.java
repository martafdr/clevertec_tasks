package by.gomel.cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * LruCache is the implementation of LRU cache. Least Recently Used (LRU) Cache
 * organizes items in order of use, allowing you to quickly identify which item
 * hasn't been used for the longest amount of time.
 */
public class LruCache<K, V> extends AbstractCache<K, V> {

    /**
     * Cache is a Map designed for storage data
     */
    private final LinkedHashMap<K, V> cacheMap;

    /**
     * The number of elements in the cache.
     */
    private final int size;

    /**
     * Constructs with the specified initial capacity.
     *
     * @throws IllegalArgumentException if the initial capacity is negative
     */
    public LruCache(int capacity) {
        size = capacity;
        if (capacity > 0) {
            cacheMap = new LinkedHashMap<>(capacity);
        } else {
            throw new IllegalArgumentException("capacity of cache can not be negative");
        }
    }

    /**
     * Save the value in cache with the specified key. If cache is full remove one element,
     * which hasn't been used for the longest amount of time.
     */
    @Override
    public void put(K key, V value) {
        if (isFull())
            cacheMap.remove(getLruKey());

        cacheMap.put(key, value);
    }

    /**
     * Returns the key of element, which hasn't been used for the longest amount of time.
     */
    private K getLruKey() {
        for (Map.Entry<K, V> unit : cacheMap.entrySet()) {
            return unit.getKey();
        }
        return null;
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or {@code null} if cache contains no mapping for the key.
     */
    @Override
    public V get(K key) {
        if (cacheMap.containsKey(key)) {
            V value = cacheMap.get(key);
            cacheMap.remove(key);
            cacheMap.put(key, value);
            return value;
        }
        return null;
    }

    /**
     * Update the value in cache to which the specified key is mapped.
     */
    @Override
    public void update(K key, V value) {
        cacheMap.remove(key);
        cacheMap.put(key, value);
    }

    /**
     * Removes the element of cache to which the specified key is mapped.
     */
    @Override
    public void remove(K key) {
        cacheMap.remove(key);
    }

    /**
     * Returns {@code true} if cache is full.
     */
    public boolean isFull() {
        return cacheMap.size() == size;
    }

    /**
     * Returns the number of elements in cache.
     */
    public int size() {
        return cacheMap.size();
    }

    /**
     * Returns {@code true} if cache contains no elements.
     */
    public boolean isEmpty() {
        return cacheMap.isEmpty();
    }

    /**
     * Returns a string representation of cache.
     */
    @Override
    public String toString() {
        return cacheMap.toString();
    }
}