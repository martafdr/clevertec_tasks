package by.gomel.entity;

import by.gomel.constants.Constants;

public class Genre {
    private long id;
    private String name;

    public Genre(String name) {
        this.name = name;
    }

    public Genre(long id, String name) {
        this(name);
        this.id = id;
    }

    public Genre() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return id + Constants.DELIMITER + name;
    }
}
