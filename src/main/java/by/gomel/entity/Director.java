package by.gomel.entity;

import by.gomel.constants.Constants;

import java.sql.Date;

public class Director {
    private long id;
    private String firstName;
    private String lastName;
    private Date birthDate;

    public Director() {
    }

    public Director(String firstName, String lastName, Date birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

    public Director(long id, String firstName, String lastName, Date birthDate) {
        this(firstName, lastName, birthDate);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return id + Constants.DELIMITER + firstName + Constants.DELIMITER +
                lastName + Constants.DELIMITER + birthDate;
    }
}
