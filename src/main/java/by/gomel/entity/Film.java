package by.gomel.entity;

import by.gomel.constants.Constants;

import java.sql.Date;

public class Film {
    private long id;
    private Director director;
    private String name;
    private Date releaseDate;
    private Genre genre;

    public Film() {
    }

    public Film(Director director, String name, Date releaseDate, Genre genre) {
        this.director = director;
        this.name = name;
        this.releaseDate = releaseDate;
        this.genre = genre;
    }

    public Film(long id, Director director, String name, Date releaseDate, Genre genre) {
        this(director, name, releaseDate, genre);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return id + Constants.DELIMITER + director + Constants.DELIMITER + name +
                Constants.DELIMITER + releaseDate + Constants.DELIMITER + genre;
    }
}
