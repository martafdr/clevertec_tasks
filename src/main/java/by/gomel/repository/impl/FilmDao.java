package by.gomel.repository.impl;

import by.gomel.dbConnector.ConnectionPool;
import by.gomel.entity.Director;
import by.gomel.entity.Film;
import by.gomel.entity.Genre;
import by.gomel.exception.NoSuchDatabaseElementException;
import by.gomel.repository.EntityDao;

import java.sql.*;
import java.util.Optional;

/**
 * DAO implementation for Entity Film
 */
public class FilmDao implements EntityDao<Film> {
    private final ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Save film in database, return id of film
     */
    @Override
    public long save(Film film) throws SQLException {
        String query = "INSERT INTO film (director_id, name, release_date, genre_id) VALUES (?,?,?,?)";
        long id = 0L;

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, film.getDirector().getId());
            preparedStatement.setString(2, film.getName());
            preparedStatement.setDate(3, film.getReleaseDate());
            preparedStatement.setLong(4, film.getGenre().getId());
            preparedStatement.execute();

            // find id
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong("id");
            }
            film.setId(id);
        }
        return id;
    }

    /**
     * Get film from database by id
     */
    @Override
    public Film get(long id) throws SQLException {
        String query = "SELECT d.id, d.first_name, d.last_name, d.birth_date, f.name, f.release_date, g.id, " +
                "g.genre_name FROM director d, film f, genre g WHERE d.id=f.director_id AND g.id=f.genre_id AND f.id = ?";
        Film film = null;
        Director director = new Director();
        Genre genre = new Genre();

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                film = new Film();
                director.setId(resultSet.getLong("id"));
                director.setFirstName(resultSet.getString("first_name"));
                director.setLastName(resultSet.getString("last_name"));
                director.setBirthDate(resultSet.getDate("birth_date"));

                film.setId(id);
                film.setDirector(director);
                film.setName(resultSet.getString("name"));
                film.setReleaseDate(resultSet.getDate("release_date"));

                genre.setId(resultSet.getLong("id"));
                genre.setName(resultSet.getString("genre_name"));

                film.setGenre(genre);
            }
        }
        return Optional.ofNullable(film).orElseThrow(NoSuchDatabaseElementException::new);
    }

    /**
     * Update film in database
     */
    @Override
    public long update(Film film) throws SQLException {
        String query = "UPDATE film SET director_id = ?, name = ?, release_date = ?, genre_id = ? WHERE id = ?";

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, film.getDirector().getId());
            preparedStatement.setString(2, film.getName());
            preparedStatement.setDate(3, film.getReleaseDate());
            preparedStatement.setLong(4, film.getGenre().getId());
            preparedStatement.setLong(5, film.getId());
            preparedStatement.execute();
        }
        return film.getId();
    }

    /**
     * Delete film in database by id
     */
    @Override
    public void delete(long id) throws SQLException {
        String query = "DELETE FROM film WHERE id = ?";

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        }
    }
}
