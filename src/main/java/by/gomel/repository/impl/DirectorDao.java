package by.gomel.repository.impl;

import by.gomel.dbConnector.ConnectionPool;
import by.gomel.entity.Director;
import by.gomel.exception.NoSuchDatabaseElementException;
import by.gomel.repository.EntityDao;

import java.sql.*;
import java.util.Optional;

/**
 * DAO implementation for Entity Director
 */
public class DirectorDao implements EntityDao<Director> {
    private final ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Save director in database, return id of director
     */
    @Override
    public long save(Director director) throws SQLException {
        String query = "INSERT INTO director (first_name, last_name, birth_date) VALUES (?,?,?)";
        long id = 0L;

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, director.getFirstName());
            preparedStatement.setString(2, director.getLastName());
            preparedStatement.setDate(3, director.getBirthDate());
            preparedStatement.execute();

            // find id
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong("id");
            }
            director.setId(id);
        }
        return id;
    }

    /**
     * Get director from database by id
     */
    @Override
    public Director get(long id) throws SQLException {
        String query = "SELECT first_name, last_name, birth_date FROM director WHERE id = ?";
        Director director = null;

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                director = new Director();
                director.setId(id);
                director.setFirstName(resultSet.getString("first_name"));
                director.setLastName(resultSet.getString("last_name"));
                director.setBirthDate(resultSet.getDate("birth_date"));
            }
        }
        return Optional.ofNullable(director).orElseThrow(NoSuchDatabaseElementException::new);
    }

    /**
     * Update director in database
     */
    @Override
    public long update(Director director) throws SQLException {
        String query = "UPDATE director SET first_name = ?, last_name = ?, birth_date = ? WHERE id = ?";

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, director.getFirstName());
            preparedStatement.setString(2, director.getLastName());
            preparedStatement.setDate(3, director.getBirthDate());
            preparedStatement.setLong(4, director.getId());;
            preparedStatement.execute();
        }
        return director.getId();
    }

    /**
     * Delete director in database by id
     */
    @Override
    public void delete(long id) throws SQLException {
        String query = "DELETE FROM director WHERE id = ?";

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        }
    }
}
