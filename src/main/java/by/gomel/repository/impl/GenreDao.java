package by.gomel.repository.impl;

import by.gomel.dbConnector.ConnectionPool;
import by.gomel.entity.Genre;
import by.gomel.exception.NoSuchDatabaseElementException;
import by.gomel.repository.EntityDao;

import java.sql.*;
import java.util.Optional;

/**
 * DAO implementation for Entity Genre
 */
public class GenreDao implements EntityDao<Genre> {
    private final ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Save genre in database, return id of genre
     */
    @Override
    public long save(Genre genre) throws SQLException {
        String query = "INSERT INTO genre (genre_name) VALUES (?)";
        long id = 0L;

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, genre.getName());
            preparedStatement.execute();

            // find id
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong("id");
            }
            genre.setId(id);
        }
        return id;
    }

    /**
     * Get genre from database by id
     */
    @Override
    public Genre get(long id) throws SQLException {
        String query = "SELECT genre_name FROM genre WHERE id = ?";
        Genre genre = null;

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                genre = new Genre();
                genre.setId(id);
                genre.setName(resultSet.getString("genre_name"));
            }
        }
        return Optional.ofNullable(genre).orElseThrow(NoSuchDatabaseElementException::new);
    }

    /**
     * Update genre in database
     */
    @Override
    public long update(Genre genre) throws SQLException {
        String query = "UPDATE genre SET genre_name = ? WHERE id = ?";

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, genre.getName());
            preparedStatement.setLong(2, genre.getId());
            preparedStatement.execute();
        }
        return genre.getId();
    }

    /**
     * Delete genre in database by id
     */
    @Override
    public void delete(long id) throws SQLException {
        String query = "DELETE FROM genre WHERE id = ?";

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        }
    }
}
