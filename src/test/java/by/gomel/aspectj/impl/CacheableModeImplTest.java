package by.gomel.aspectj.impl;

import by.gomel.entity.Genre;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CacheableModeImplTest {
    @Mock
    private final CacheableModeImpl cacheableMode = mock(CacheableModeImpl.class);
    private final ProceedingJoinPoint joinPoint = mock(ProceedingJoinPoint.class);
    private final Genre genre = new Genre("tragicomedy");

    @Test
    void getFromCacheOrDao() throws Throwable {
        when(cacheableMode.getFromCacheOrDao(joinPoint)).thenReturn(genre);
        assertEquals(genre, cacheableMode.getFromCacheOrDao(joinPoint), "If cache have element with key joinPoint.getArgs() - " +
                "return it from cache, if not - return joinPoint.proceed()");
    }

    @Test
    void remove() throws Throwable {
        assertNull(cacheableMode.remove(joinPoint),
                "Remove from cache element with key joinPoint.getArgs()");
    }
}