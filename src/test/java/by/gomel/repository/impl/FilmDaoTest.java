package by.gomel.repository.impl;

import by.gomel.entity.Director;
import by.gomel.entity.Film;
import by.gomel.entity.Genre;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Date;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FilmDaoTest {
    @Mock
    private final FilmDao dao = mock(FilmDao.class);

    private Film film;
    private Director director;
    private Genre genre;

    @BeforeEach
    public void initializeEntities() {
        director = new Director(11, "Peter", "Farrelly", Date.valueOf("1956-12-17"));
        genre = new Genre(1, "tragicomedy");
        film = new Film(15, director, "Red Book", Date.valueOf("2018-09-11"), genre);
    }

    @AfterEach
    public void makeEntitiesNull() {
        film = null;
        director = null;
        genre = null;
    }

    @Test
    void save() throws SQLException {
        when(dao.save(film)).thenReturn(film.getId());
        assertEquals(film.getId(), dao.save(film));
    }

    @Test
    void get() throws SQLException {
        when(dao.get(film.getId())).thenReturn(film);
        assertEquals(film, dao.get(film.getId()));
    }

    @Test
    void update() throws SQLException {
        film.setName("Green Book");
        dao.update(film);
        when(dao.update(film)).thenReturn(film.getId());
        assertEquals(film.getId(), dao.update(film));
    }

    @Test
    void delete() throws SQLException {
        dao.delete(film.getId());
        film = new Film();
        when(dao.get(film.getId())).thenReturn(film);
        assertEquals(film, dao.get(film.getId()));
    }
}