package by.gomel.repository.impl;

import by.gomel.entity.Director;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Date;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DirectorDaoTest {
    @Mock
    private final DirectorDao dao = mock(DirectorDao.class);

    private Director director;

    @BeforeEach
    public void initializeDirector() {
        director = new Director(11, "Peter", "Farrelly", Date.valueOf("1956-12-17"));
    }

    @AfterEach
    public void makeDirectorNull() {
        director = null;
    }

    @Test
    void save() throws SQLException {
        when(dao.save(director)).thenReturn(director.getId());
        assertEquals(director.getId(), dao.save(director));
    }

    @Test
    void get() throws SQLException {
        when(dao.get(director.getId())).thenReturn(director);
        assertEquals(director, dao.get(director.getId()));
    }

    @Test
    void update() throws SQLException {
        director.setFirstName("Bob");
        when(dao.update(director)).thenReturn(director.getId());
        assertEquals(director.getId(), dao.update(director));
    }

    @Test
    void delete() throws SQLException {
        dao.delete(director.getId());
        director = new Director();
        when(dao.get(director.getId())).thenReturn(director);
        assertEquals(director, dao.get(director.getId()));
    }
}