package by.gomel.repository.impl;

import by.gomel.entity.Genre;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GenreDaoTest {
    @Mock
    private final GenreDao dao = mock(GenreDao.class);

    private Genre genre;

    @BeforeEach
    public void initializeGenre() {
        genre = new Genre(1, "drama");
    }

    @AfterEach
    public void makeGenreNull() {
        genre = null;
    }

    @Test
    void save() throws SQLException {
        when(dao.save(genre)).thenReturn(genre.getId());
        assertEquals(genre.getId(), dao.save(genre));
    }

    @Test
    void get() throws SQLException {
        when(dao.get(genre.getId())).thenReturn(genre);
        assertEquals(genre, dao.get(genre.getId()));
    }

    @Test
    void update() throws SQLException {
        genre.setName("crime");
        when(dao.update(genre)).thenReturn(genre.getId());
        assertEquals(genre.getId(), dao.update(genre));
    }

    @Test
    void delete() throws SQLException {
        dao.delete(genre.getId());
        genre = new Genre();
        when(dao.get(genre.getId())).thenReturn(genre);
        assertEquals(genre, dao.get(genre.getId()));
    }
}