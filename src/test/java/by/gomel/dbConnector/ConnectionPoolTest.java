package by.gomel.dbConnector;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class ConnectionPoolTest {

    @Test
    void getInstance() {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        assertNotNull(connectionPool);
    }

    @Test
    void getConnection() throws SQLException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        assertNotNull(connection);
    }

    @Test
    void createTwoConnections_mustBeNotEquals() throws SQLException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Connection connection1 = ConnectionPool.getInstance().getConnection();
        connection1.close();
        assertNotEquals(connection, connection1);
    }

    @Test
    void createTwoConnectionsAndCloseOne_mustBeNotEquals() throws SQLException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Connection connection1 = ConnectionPool.getInstance().getConnection();
        connection1.close();
        assertNotEquals(connection, connection1);
    }
}