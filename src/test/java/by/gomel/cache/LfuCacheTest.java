package by.gomel.cache;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LfuCacheTest {
    LfuCache<Integer, Integer> cache = new LfuCache<>(3);

    @BeforeEach
    public void testAddDataInCache() {
        cache.put(1, 1);
        cache.put(2, 2);
        cache.put(3, 3);
        assertEquals(3, cache.size());
    }

    @AfterEach
    public void createEmptyCache() {
        cache = new LfuCache<>(3);
        assertTrue(cache.isEmpty());
    }

    @Test
    public void testGet(){
        assertEquals(1, cache.get(1));
    }

    @Test
    public void testUpdate(){
        cache.update(1, 11);
        assertEquals(11, cache.get(1));
    }

    @Test
    public void testRemove(){
        cache.remove(2);
        assertNull(cache.get(2));
    }

    @Test
    public void testPutInFullCache(){
        cache.put(4, 4);
        assertNull(cache.get(1));
    }

    @Test
    public void testLFU(){
        cache.get(3);
        cache.get(1);
        cache.get(1);
        cache.get(3);
        cache.update(2,0);
        cache.put(4, 4);
        assertNull(cache.get(2));
    }
}