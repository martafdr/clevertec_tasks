package by.gomel.collection;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class ConcurrencyQueueTest {
    private final Logger logger = LoggerFactory.getLogger(ConcurrencyQueueTest.class);
    ConcurrencyQueue<Integer> concurrencyQueue = new ConcurrencyQueue<>();
    PriorityBlockingQueue<Integer> priorityBlockingQueue = new PriorityBlockingQueue<>();

    @BeforeEach
    public void testAddDataInCollections() {
        concurrencyQueue.add(1);
        concurrencyQueue.add(2);
        concurrencyQueue.add(3);
        assertEquals(3, concurrencyQueue.size());
    }

    @AfterEach
    public void testClear() {
        concurrencyQueue.clear();
        priorityBlockingQueue.clear();
        assertEquals(0, concurrencyQueue.size());
    }

    @Test
    public void testAdd() {
        assertTrue(concurrencyQueue.add(5));
    }

    @Test
    public void testAddWithSeveralThreats(){
        Thread t1 = new Thread(() -> IntStream.range(0, 5000).forEach(concurrencyQueue::add));
        Thread t2 = new Thread(() -> IntStream.range(0, 5000).forEach(concurrencyQueue::add));
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            logger.error(e.toString());
        }
        assertEquals(10003, concurrencyQueue.size());
    }

    @Test
    public void testAddWithSeveralThreatsComparedWithPriorityQueue(){
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        Thread t1 = new Thread(() -> IntStream.range(0, 500_000).forEach(concurrencyQueue::add));
        Thread t2 = new Thread(() -> IntStream.range(0, 500_000).forEach(priorityQueue::add));
        try {
            t1.start();
            t2.start();

            t1.join();
            t2.join();
        } catch (InterruptedException | NullPointerException e) {
            logger.error(e.toString());
        }
        assertNotEquals(priorityQueue.size(), concurrencyQueue.size(), "Priority Queue is not concurrent! \n" +
                "In priority queue result will be less than in concurrency queue and can be NullPointerException");
    }

    @Test
    public void testOfferNull() {
        assertThrows(NullPointerException.class, () -> concurrencyQueue.offer(null));
    }

    @Test
    public void testRemove() {
        priorityBlockingQueue.addAll(concurrencyQueue);
        assertEquals(priorityBlockingQueue.remove(), concurrencyQueue.remove());
    }

    @Test
    public void testPoll() {
        priorityBlockingQueue.addAll(concurrencyQueue);
        assertEquals(priorityBlockingQueue.poll(), concurrencyQueue.poll());
    }

    @Test
    public void testSizeAfterRemove() {
        int sizeBeforeRemove = concurrencyQueue.size();
        concurrencyQueue.remove();
        assertEquals(sizeBeforeRemove - 1, concurrencyQueue.size());
    }

    @Test
    public void testRemoveWithParameter() {
        int element = concurrencyQueue.element();
        concurrencyQueue.add(4);
        assertTrue(concurrencyQueue.remove(element));
    }

    @Test
    public void testRemoveInEmptyMyQueue() {
        concurrencyQueue.clear();
        assertThrows(NoSuchElementException.class, () -> concurrencyQueue.remove());
    }

    @Test
    public void testPollInEmptyMyQueue() {
        concurrencyQueue.clear();
        assertNull(concurrencyQueue.poll());
    }

    @Test
    public void testSizeAfterAddAll() {
        List<Integer> list = Arrays.asList(2, 3, 4, 5, 6, 22, 33, 75, 8, 11, 73, 44, 26);
        int sizeBeforeAddAll = concurrencyQueue.size();
        concurrencyQueue.addAll(list);
        assertEquals(sizeBeforeAddAll + list.size(), concurrencyQueue.size());
    }

    @Timeout(1000)
    @Test
    public void testElement() {
        priorityBlockingQueue.addAll(concurrencyQueue);
        assertEquals(priorityBlockingQueue.element(), concurrencyQueue.element());
    }

    @Test
    public void testElementInEmptyMyQueue() {
        concurrencyQueue.clear();
        assertThrows(NoSuchElementException.class, () -> concurrencyQueue.remove());
    }

    @Timeout(1000)
    @Test
    public void testPeek() {
        priorityBlockingQueue.addAll(concurrencyQueue);
        assertEquals(priorityBlockingQueue.peek(), concurrencyQueue.peek());
    }

    @Test
    public void testContains_returnTrue() {
        concurrencyQueue.add(7);
        assertTrue(concurrencyQueue.contains(7));
    }

    @Test
    public void testContains_returnFalse() {
        int x = concurrencyQueue.stream().max(Integer::compareTo).get() + 1;
        // x don't present in concurrencyQueue (x > then max element of concurrencyQueue)
        assertFalse(concurrencyQueue.contains(x));
    }

    @Test
    public void testContainsAll_returnTrue() {
        priorityBlockingQueue.addAll(concurrencyQueue);
        assertTrue(concurrencyQueue.containsAll(priorityBlockingQueue));
    }

    @Test
    public void testContainsAll_returnFalse() {
        int x = concurrencyQueue.stream().max(Integer::compareTo).get() + 1;
        priorityBlockingQueue.add(x); // x don't present in concurrencyQueue (x > then max element of concurrencyQueue)
        assertFalse(concurrencyQueue.containsAll(priorityBlockingQueue));
    }

    @Test
    public void testRemoveAll_returnTrue() {
        priorityBlockingQueue.addAll(concurrencyQueue);
        assertTrue(concurrencyQueue.removeAll(priorityBlockingQueue));
    }

    @Test
    public void testRemoveAll_makeQueueEmpty() {
        priorityBlockingQueue.addAll(concurrencyQueue);
        concurrencyQueue.removeAll(priorityBlockingQueue);
        assertTrue(concurrencyQueue.isEmpty());
    }

    @Test
    public void testRetainAll_returnTrue() {
        priorityBlockingQueue.addAll(concurrencyQueue);
        concurrencyQueue.add(44);
        concurrencyQueue.add(15);
        assertTrue(concurrencyQueue.retainAll(priorityBlockingQueue));
    }

    @Test
    public void testRetainAll_differentFromRetainingValuesWillBeRemoved() {
        priorityBlockingQueue.addAll(concurrencyQueue);
        concurrencyQueue.add(44);
        concurrencyQueue.retainAll(priorityBlockingQueue);
        assertFalse(concurrencyQueue.contains(44));
    }

    @Test
    public void testForEach() {
        concurrencyQueue.forEach((Integer element) -> priorityBlockingQueue.add(element));
        assertAll(
                () -> assertEquals(priorityBlockingQueue.size(), concurrencyQueue.size()),
                () -> assertEquals(priorityBlockingQueue.element(), concurrencyQueue.element()),
                () -> assertTrue(priorityBlockingQueue.containsAll(concurrencyQueue))
        );
    }
}
