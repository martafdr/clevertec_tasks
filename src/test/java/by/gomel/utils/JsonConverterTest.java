package by.gomel.utils;

import by.gomel.utils.converter.JsonConverter;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;
public class JsonConverterTest {
    JsonConverter converter = new JsonConverter();
    Cat cat;

    @Test
    public void convert(){
        cat = new Cat("Tom", 3);

        String regexp = "\\{\\s*\"name\"\\s*:\\s*\"Tom\"\\s*,\\s*\"age\"\\s*:\\s*3\\s*}";
        Pattern pattern = Pattern.compile(regexp);
        String result = converter.convert(cat);
        Matcher matcher = pattern.matcher(result);
        assertTrue(matcher.find(), "The result matches the regular expression");
    }

    @Test
    public void convertNull(){
        String regexp = "\\{\\s*}";
        Pattern pattern = Pattern.compile(regexp);
        String result = converter.convert(null);
        Matcher matcher = pattern.matcher(result);
        assertTrue(matcher.find(), "The result contains { } and can contain any whitespace character(s)");
    }

    @Test
    public void getSchema(){
        cat = new Cat();
        String regexp = "\"title\"\\s*:\\s*.*\\s*,\\s*\"type\"\\s*:\\s*\"object\"\\s*,\\s*\"properties\"\\s*:";
        Pattern pattern = Pattern.compile(regexp);
        String schema = converter.getSchema(cat);
        Matcher matcher = pattern.matcher(schema);
        assertTrue(matcher.find(), "The schema contains title, type and properties");
    }
}

class Cat {
    private String name;
    private int age;

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Cat(){}
}