# About application

It is a Java-based application, which have been created in educational purposes.
In this application, you can find great films from outstanding directors such as Quentin Tarantino, Christopher Nolan, Guy Ritchie, Luc Besson.

### Preconditions
1. JDK 8+
2. Gradle
3. PostgreSQL

### Clone project to your computer

    git clone https://martafdr@bitbucket.org/martafdr/clevertec_tasks.git
Clones a repository to your computer

### Running the application

Before running the application you must connect to database
There is the file with properties of application, where you must write your url, login, password
`src/main/resources/application.yml`
then test the connection with database

If it is ok, then create database object and add data by execution of this file:
`src/main/resources/db/creator.sql`

Run the application in Runner class
`src/main/java/by/gomel/Runner.java`

or run the application in the root directory by
`gradle run`